# ARIA Data sources subscription framework

Data sources subscription framework for use in ARIA and related microservices.

## Installation

`composer require aria-php/aria-data-subscription`

## Usage 

The `DatasourceController` handles the execution of the various plugins, this is what you execute from your
cron job.

The `Datasource` describes the specific implementation that contacts the appropriate feed.

The various `Model` classes are intended to be implemented to wrap around the appropriate database / table structures that are specific 
to ARIA core or the microservice that actually executes the subscription query.

