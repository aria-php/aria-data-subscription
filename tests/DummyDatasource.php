<?php

namespace ARIA\DataSource\Tests; 
use ARIA\DataSource\model\DatasourceModel;
use ARIA\DataSource\Datasource;

class DummyDatasource extends Datasource {


  public function execute(): bool 
  {
    return true;
  }
}