<?php

namespace ARIA\DataSource\Tests; 

use ARIA\DataSource\model\DatasourceControllerModel;
use ARIA\DataSource\Datasource;
use Generator;

class DummyDatasourceControllerModel extends DatasourceControllerModel
{

  private array $datasource = [];

  public function feeds(array $parameters = [], int $limit = 10, int $offset = 0): ?Generator {

    foreach ($this->datasource as $datasource) {
      yield $datasource;
    }
  }

  public function setFeed(
    Datasource $datasource
  ): bool 
  {
    $this->datasource[] = $datasource;

    return true;
  }
}
