<?php

namespace ARIA\DataSource\Tests; 

use ARIA\DataSource\DatasourceController;
use ARIA\DataSource\Datasource;
use \PHPUnit\Framework\TestCase;
use ARIA\DataSource\Tests\DummyDatasourceControllerModel;
use ARIA\DataSource\Tests\DummyDatasourceModel;

class DatasourceControllerTest extends TestCase {

  private $datasourceController;

  public function setUp() :void {
    $this->datasourceController = new DatasourceController(
      new DummyDatasourceControllerModel()
    );
  }

  public function testStore() {

    $datasource = Datasource::factory(DummyDatasource::class, new DummyDatasourceModel());
    
    $this->assertInstanceOf( DummyDatasource::class, $datasource);

    $datasource->setFeedURL('http://example.com/feed');
    $datasource->setTitle('Example Title');
    $datasource->setInfo('Example info');
    $datasource->setPeriod(Datasource::PERIOD_DAILY);

    $this->assertTrue($datasource->save());

    $this->assertTrue($this->datasourceController->getModel()->setFeed($datasource)); // for the purposes of the test, lets "store" this value in a way that can be retrieved

  }

  public function testFeed() {
    
    $this->testStore();
    $found = false;

    foreach ($this->datasourceController->feeds() as $feed) {
      $found = true;
    }

    $this->assertTrue($found);
  }


}