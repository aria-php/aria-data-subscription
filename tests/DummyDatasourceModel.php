<?php

namespace ARIA\DataSource\Tests; 
use ARIA\DataSource\model\DatasourceModel;
use ARIA\DataSource\Datasource;

class DummyDatasourceModel extends DatasourceModel {

  private $data = [

  ];


  public function load(int $id) : bool {
    return true;
  }

  public function store(Datasource $datastore) : bool {

    $this->data['id'] = $datastore->getID();
    $this->data['feed'] = $datastore->getFeedURL();
    $this->data['title'] = $datastore->getTitle();
    $this->data['info'] = $datastore->getInfo();
    $this->data['period'] = $datastore->getPeriod();
    $this->data['data'] = $datastore->getData();

    return true;
  }
}