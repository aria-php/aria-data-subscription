<?php

namespace ARIA\DataSource;

use ARIA\DataSource\model\DatasourceControllerModel;
use Generator;

/**
 * Controller class for managing the data source service
 */
class DatasourceController {

  private DatasourceControllerModel $model;


  public function __construct(DatasourceControllerModel $model)
  {
    $this->setModel($model);
  }

  public function getModel() : ?DatasourceControllerModel 
  {
    return $this->model;
  }

  public function setModel(DatasourceControllerModel $model) {
    $this->model = $model;
  }

  /**
   * Iterate through the available feeds.
   *
   * @param array $parameters
   * @param integer $limit
   * @param integer $offset
   * @return Generator
   */
  function feeds(array $parameters = [], int $limit = 10, int $offset = 0)  : Generator
  {
    return $this->model->feeds($parameters, $limit, $offset);
  }


  
  /**
   * Execute all currently active feeds.
   *
   * @return boolean
   * @throws DatasourceException
   */
  public function execute( int $period = Datasource::PERIOD_DAILY) : bool {

    foreach ($this->feeds([
      'active' => true,
      'period' => $period
    ]) as $feed) {

      $feed->execute();

    }

    return true;
  }
}