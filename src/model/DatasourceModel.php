<?php

namespace ARIA\DataSource\model;

use ARIA\DataSource\Datasource;

abstract class DatasourceModel {

  private int $id = 0;

  private string $feedurl = '';

  private string $title = '';
  private string $info = '';

  private int $period = Datasource::PERIOD_DAILY;
  private bool $active = true;

  private array $data = [];

  public function getID(): int
  {
    return $this->id;
  }

  public function setFeedURL(string $feed)
  {
    $this->feedurl = $feed;
  }

  public function getFeedURL(): string
  {
    return $this->feedurl;
  }

  public function setTitle(string $title)
  {
    $this->title = $title;
  }

  public function getTitle(): string
  {
    return $this->title;
  }

  public function setInfo(string $info)
  {
    $this->info = $info;
  }

  public function getInfo(): string
  {
    return $this->info;
  }

  public function setPeriod(int $period)
  {
    $this->period = $period;
  }

  public function getPeriod(): string
  {
    return $this->period;
  }

  public function setActive(bool $active)
  {
    $this->active = $active;
  }

  public function getActive(): string
  {
    return $this->active;
  }

  public function setData(array $data)
  {
    $this->data = $data;
  }

  public function getData(): array
  {
    return $this->data;
  }

  abstract public function store(Datasource $datastore) : bool;

  abstract public function load(int $id) : bool;

}