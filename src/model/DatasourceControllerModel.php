<?php

namespace ARIA\DataSource\model;

use ARIA\DataSource\Datasource;
use Generator;

abstract class DatasourceControllerModel {

  /**
   * Iterate over feeds
   *
   * @param array $parameters
   * @param integer $limit
   * @param integer $offset
   * @return Generator|null
   */
  abstract public function feeds(array $parameters = [], int $limit = 10, int $offset = 0): ? Generator;

}