<?php

namespace ARIA\DataSource;

use ARIA\DataSource\model\DatasourceModel;
use RuntimeException;

/**
 * Abstract definition of a data source
 */
abstract class Datasource
{

  const PERIOD_MANUAL = 0; // Should not be triggered by CRON, this should be triggered manually
  const PERIOD_HOURLY = 1;
  const PERIOD_DAILY = 2;
  const PERIOD_WEEKLY = 3;

  private DatasourceModel $model;

  public function setModel(DatasourceModel $model)
  {
    $this->model = $model;
  }

  public function getModel(): ?DatasourceModel
  {
    return $this->model;
  }

  public function getID(): int
  {
    return $this->model->getID();
  }

  public function setFeedURL(string $feed)
  {
    $this->model->setFeedURL($feed);
  }

  public function getFeedURL(): string
  {
    return $this->model->getFeedURL();
  }

  public function setTitle(string $title)
  {
    $this->model->setTitle($title);
  }

  public function getTitle(): string
  {
    return $this->model->getTitle();
  }

  public function setInfo(string $info)
  {
    $this->model->setInfo($info);
  }

  public function getInfo(): string
  {
    return $this->model->getInfo();
  }

  public function setPeriod(int $period)
  {
    $this->model->setPeriod($period);
  }

  public function getPeriod(): string
  {
    return $this->model->getPeriod();
  }

  public function setActive(bool $active)
  {
    $this->model->setActive($active);
  }

  public function getActive(): string
  {
    return $this->model->getActive();
  }

  public function setData(array $data)
  {
    $this->model->setData($data);
  }

  public function getData(): array
  {
    return $this->model->getData();
  }

  public function getCustom(): ?array
  {
    $custom = json_decode($this->getData()['custom'], true);
    if (!empty($custom)) {
      return $custom;
    }

    if ($custom === null) {
      throw new RuntimeException('Custom data wasn\'t valid json');
    }

    return null;
  }

  /**
   * Retrieve a default external reference key for a given remote entity.
   * 
   * This remote key is constructed out of feed:ref, and serves as the default reference for an item. This should
   * be used as a fallback, ideally you'll be wanting to use something like a PID / DOI
   *
   * @param [type] $remote_entity_id
   * @return string
   */
  protected function mintDefaultEntityRef($remote_entity_id) : string {

    return $this->getFeedURL() . ":" . $remote_entity_id;

  }

  /**
   * Store this data source to the underlaying handler.
   *
   * @return void
   */
  public function save() : bool
  {
    return $this->model->store($this);
  }

  /**
   * Populate datasource with loaded value
   *
   * @param integer $id
   * @return boolean
   */
  public function load(int $id) : bool {
    return $this->model->load($id);
  }

  /**
   * Execute this datasource.
   *
   * @return boolean
   * @throws DatasourceException
   */
  public abstract function execute(): bool;

  /**
   * Create new empty Datasource with a given handler.
   *
   * @param string $handler
   * @return Datasource
   */
  public static function factory(string $handler, DatasourceModel $datamodel) : Datasource {

    $datasource = new $handler();
    
    if (!$datasource instanceof Datasource) {
      throw new DatasourceException("{$handler} is not a valid Datasource");
    }

    $datasource->setModel($datamodel);

    return $datasource;
  }
}
